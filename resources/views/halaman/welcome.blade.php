@extends('tata-letak.main')

@section('judul')
    Welcome
@endsection

@section('konten')
    <h1>SELAMAT DATANG! {{$fname}} {{$lname}}</h1>
    <h4>Terima kasih telah bergabung di website kami.<br>Media belajar kita bersama!</h4>
@endsection
