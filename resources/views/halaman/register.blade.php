@extends('tata-letak.main')

@section('judul')
    Register
@endsection

@section('konten')
    <h2>Buat Account Baru</h2>
    <h4>Sign Up Form</h4>
    <form action="/welcome" method="post">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="fname"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="lname"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender" id="m" value="m"><label for="m">Male</label><br>
        <input type="radio" name="gender" id="f" value="f"><label for="f">Female</label><br><br>
        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="id">Indonesia</option>
            <option value="ps">Palestine</option>
            <option value="sa">Saudi Arabia</option>
        </select><br><br>
        <label>Language spoken:</label><br><br>
        <input type="checkbox" name="lang1" id="id" value="id"><label for="id">Bahasa Indonesia</label><br>
        <input type="checkbox" name="lang2" id="en" value="en"><label for="en">English</label><br>
        <input type="checkbox" name="lang3" id="xx" value="xx"><label for="xx">Other</label><br><br>
        <label>Bio:</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection
