@extends('tata-letak.main')

@section('judul')
  Edit Cast
@endsection

@section('konten')
  <form action="/cast/{{$cast->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
      <label for="nama">Nama</label>
      <input type="text" class="form-control" id="nama" name="nama" value="{{$cast->nama}}">
    </div>
    @error('nama')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label for="umur">Umur</label>
      <input type="number" class="form-control" id="umur" name="umur" value="{{$cast->umur}}">
    </div>
    @error('umur')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label for="bio">Bio</label>
      <textarea class="form-control" id="bio" rows="10" name="bio">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
