@extends('tata-letak.main')

@section('judul')
  Detail Cast
@endsection

@section('konten')
  <h1>{{$cast->nama}}</h1>
  <h3 class="mb-3">{{$cast->umur}} tahun</h3>
  <p>{{$cast->bio}}</p>
@endsection
