@extends('tata-letak.main')

@section('judul')
  Detail Genre
@endsection

@section('konten')
  <h1 class="mb-3">{{$genre->nama}}</h1>
  <div class="row">
    @forelse ($genre->film as $item)
      <div class="col-4">
        <div class="card">
          <img src="{{asset('gambar/' . $item->poster)}}" class="card-img-top" alt="">
          <div class="card-body">
            <h3>{{$item->judul}}</h3>
            <h4 class="card-subtitle mb-2 text-muted">{{$item->tahun}}</h4>
            <p class="card-text">{{Str::limit($item->ringkasan, 160, $end=' …')}}</p>
            <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
          </div>
        </div>
      </div>
    @empty
      <h1>Data tidak ditemukan.</h1>
    @endforelse
  </div>
@endsection
