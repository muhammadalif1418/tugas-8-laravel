@extends('tata-letak.main')

@push('style')
  <style>
    td:last-child .btn {
      width: 56px;
    }
  </style>
@endpush

@section('judul')
  Daftar Genre
@endsection

@section('konten')
  @auth
  <a href="/genre/create" class="btn btn-success mb-3">Tambah Genre</a>
  @endauth
  <table class="table table-striped">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        {{-- <th scope="col">Aksi</th> --}}
        <th scope="col"
              @auth
                  style="width: 200px;"
                      @endauth
                          @guest
                              style="width: 82px;"
                                  @endguest
                                      >Aksi</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($genre as $key => $item)
        <tr>
          <td>{{$key + 1}}</td>
          <td>{{$item->nama}}</td>
          {{-- <td
              @auth
                  style="width: 200px;"
                      @endauth
                          > --}}
          <td>
            @auth
            <form action="/genre/{{$item->id}}" method="post">
              <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
              <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
              @csrf
              @method('delete')
              <input type="submit" class="btn btn-danger btn-sm" value="Hapus">
            </form>
            @endauth
            @guest
            <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
            @endguest
          </td>
        </tr>
      @empty
        <h1 class="mb-4">Data tidak ditemukan.</h1>
      @endforelse
    </tbody>
  </table>
@endsection
