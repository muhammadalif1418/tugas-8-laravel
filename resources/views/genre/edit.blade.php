@extends('tata-letak.main')

@section('judul')
  Edit Genre
@endsection

@section('konten')
  <form action="/genre/{{$genre->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
      <label for="nama">Nama</label>
      <input type="text" class="form-control" id="nama" name="nama" value="{{$genre->nama}}">
    </div>
    @error('nama')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
