@extends('tata-letak.main')

@section('judul')
  Edit Profile
@endsection

@section('konten')
  <form action="/profile/{{$profile->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
      <label for="name">Nama</label>
      <input type="text" class="form-control" id="name" value="{{$profile->user->name}}" disabled>
    </div>
    <div class="form-group">
      <label for="email">Email</label>
      <input type="text" class="form-control" id="email" value="{{$profile->user->email}}" disabled>
    </div>

    <div class="form-group">
      <label for="umur">Umur</label>
      <input type="number" class="form-control" id="umur" name="umur" value="{{$profile->umur}}">
    </div>
    @error('umur')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label for="bio">Bio</label>
      <textarea class="form-control" id="bio" name="bio">{{$profile->bio}}</textarea>
    </div>
    @error('bio')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label for="alamat">Alamat</label>
      <textarea class="form-control" id="alamat" name="alamat">{{$profile->alamat}}</textarea>
    </div>
    @error('alamat')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
