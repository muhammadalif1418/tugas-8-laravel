@extends('tata-letak.main')

@section('judul')
  Detail Film
@endsection

@section('konten')
  <div class="row mb-4">
    <div class="col-6 pr-4">
      <h1>{{$film->judul}}</h1>
      <h3 class="mb-3">{{$film->tahun}}</h3>
      <p class="mb-4">{{$film->ringkasan}}</p>
      {{-- <a href="/film" class="btn btn-secondary mt-3">Kembali</a> --}}
      @foreach ($film->kritik as $item)
        <div class="card">
          <div class="card-header">
            {{$item->user->name}}
            {{-- {{$item->user->name}} <span style="opacity: .6";>memberi point</span> {{$item->point}} --}}
          </div>
          <div class="card-body">
            <h5 class="card-text mb-3">{{$item->isi}}</h5>
            {{-- <h5 class="card-title"><b>{{$item->point}}</b></h5> --}}
            <h5 class="card-title">
              @for ($i = 0; $i < $item->point; $i++)
                <i class="nav-icon fas fa-star"></i>
              @endfor
            </h5>
            {{-- <a href="#" class="btn btn-primary">Go somewhere</a> --}}
          </div>
        </div>
      @endforeach
    </div>
    {{-- <div class="col-1"></div> --}}
    <div class="col-6 pl-4">
      <img src="{{asset('gambar/' . $film->poster)}}" alt="" class="img-fluid mb-3">
      @auth
      <form action="/kritik" method="post">
        @csrf
        <input type="hidden" name="film_id" value="{{$film->id}}">
        <div class="form-group">
          <label for="isi">Kritik</label>
          <textarea name="isi" id="isi" class="form-control"></textarea>
        </div>
        @error('isi')
          <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
          <label for="point">Point</label>
          <select class="form-control custom-select" id="point" name="point">
            <option value="" disabled selected hidden></option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            {{-- <option value="6">6</option>
            <option value="7">7</option>
            <option value="8">8</option>
            <option value="9">9</option>
            <option value="10">10</option> --}}
          </select>
        </div>
        @error('point')
          <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
      @endauth
    </div>
  </div>
  {{-- <a href="/film" class="btn btn-secondary">Kembali</a> --}}
@endsection
