@extends('tata-letak.main')

@push('style')
  <style>
    .col-4 .btn {
      width: 56px;
    }
  </style>
@endpush

@section('judul')
  Daftar Film
@endsection

@section('konten')
  @auth
  <a href="/film/create" class="btn btn-success mb-3">Tambah Film</a>
  @endauth
  <div class="row">
    @forelse ($film as $item)
      <div class="col-4">
        <div class="card">
          <img src="{{asset('gambar/' . $item->poster)}}" class="card-img-top" alt="">
          <div class="card-body">
            <span class="badge badge-success mb-2">{{$item->genre->nama}}</span>
            <h3>{{$item->judul}}</h3>
            <h4 class="card-subtitle mb-2 text-muted">{{$item->tahun}}</h4>
            <p class="card-text">{{Str::limit($item->ringkasan, 160, $end=' …')}}</p>
            @auth
            <form action="/film/{{$item->id}}" method="post">
              <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
              <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
              @csrf
              @method('delete')
              <input type="submit" class="btn btn-danger btn-sm" value="Hapus">
            </form>
            @endauth
            @guest
              <a href="/film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
            @endguest
          </div>
        </div>
      </div>
    @empty
      <h1>Data tidak ditemukan.</h1>
    @endforelse
  </div>
@endsection
