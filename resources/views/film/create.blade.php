@extends('tata-letak.main')

@push('script')
  <script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>
  <script>
    $(document).ready(function() {
      bsCustomFileInput.init();
    });
  </script>
@endpush

@section('judul')
  Tambah Film
@endsection

@section('konten')
  <form action="/film" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label for="judul">Judul</label>
      <input type="text" class="form-control" id="judul" name="judul">
    </div>
    @error('judul')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label for="ringkasan">Ringkasan</label>
      <textarea class="form-control" id="ringkasan" rows="10" name="ringkasan"></textarea>
    </div>
    @error('ringkasan')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label for="tahun">Tahun</label>
      <input type="number" class="form-control" id="tahun" name="tahun">
    </div>
    @error('tahun')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label for="genre">Genre</label>
      {{-- <input type="number" class="form-control" id="genre" name="genre_id"> --}}
      <select class="form-control custom-select" id="genre" name="genre_id">
        {{-- <option value="" disabled selected hidden>Pilih …</option> --}}
        <option value="" disabled selected hidden></option>
        @foreach ($genre as $item)
          <option value="{{$item->id}}">{{$item->nama}}</option>
        @endforeach
      </select>
    </div>
    @error('genre_id')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      {{-- <label for="poster">Poster</label> --}}
      <label>Poster</label>
      {{-- <input type="file" class="form-control" id="poster" name="poster"> --}}
      <div class="custom-file">
        <input type="file" class="custom-file-input" id="poster" name="poster">
        {{-- <label class="custom-file-label" for="poster">Pilih berkas</label> --}}
        <label class="custom-file-label" for="poster"></label>
      </div>
    </div>
    @error('poster')
      <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
